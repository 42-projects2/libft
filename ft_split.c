/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmarie-- <tmarie--@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/05 11:25:55 by tmarie--          #+#    #+#             */
/*   Updated: 2022/01/05 12:27:38 by tmarie--         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	**free_all(char **array)
{
	unsigned int	i;

	i = 0;
	while (array[i])
	{
		free(array[i]);
		i++;
	}
	free(array);
	return (NULL);
}


static unsigned int ft_get_nb_col(char const *s, char c)
{
	

static void	ft_get_row(char **row, unsigned int *len_row, char c)
{


char	**ft_split(char cont *s, char c)
{
	char	**new;
	char	*copy;
	unsigned int	i;
	unsigned int	nb_col;
	unsigned int	len_row;

	copy = (char *)s;
	len_row = 0;
	i = 0;
	nb_col = ft_get_nb_col(s, c);
	new = malloc(sizeof(char *) * (nb_col + 1));
	if (new == NULL)
		return (NULL);
	while (i < nb_col)
	{
		ft_get_row(&copy, &len_row, c);
		new[i] = malloc(sizeof(char) * (len_row + 1));
		if (new[i] == NULL)
			return (free_all(new));
		ft_strlcpy(new[i], copy,len_row + 1);
		i++;
	}
	new[i] = NULL;
	return (new);
}


/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmarie-- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/27 16:57:11 by tmarie--          #+#    #+#             */
/*   Updated: 2021/11/29 18:42:39 by tmarie--         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	i;
	size_t	j;
	char	*srcbis;

	i = 0;
	j = 0;
	srcbis = (char *)src;
	while (dst[i] && i < size)
		i++;
	if (i == size)
		return (i + ft_strlen(srcbis));
	if (size > i)
	{
		while (j < size - i - 1 && srcbis[j])
		{
			dst[i + j] = srcbis[j];
			j++;
		}
		dst[i + j] = '\0';
	}
	return (i + ft_strlen(srcbis));
}

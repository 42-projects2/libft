/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmarie-- <tmarie--@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/01 17:29:45 by tmarie--          #+#    #+#             */
/*   Updated: 2021/12/01 18:40:59 by tmarie--         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	int		i;
	int		j;
	int		k;
	char	*str;

	i = ft_strlen((char *)s1);
	j = ft_strlen((char *)s2);
	str = malloc(sizeof(char) * (i + j + 1));
	if (str == 0)
		return (NULL);
	k = 0;
	while (k < i)
	{
		str[k] = s1[k];
		k++;
	}
	while (k - i < j)
	{
		str[k] = s2[k - i];
		k++;
	}
	str[k] = 0;
	return (str);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmarie-- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 11:36:19 by tmarie--          #+#    #+#             */
/*   Updated: 2021/12/14 09:32:00 by tmarie--         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	int	i;

	i = 0;
	if (c == 0)
	{
		return ((char *)&s[ft_strlen((char *)s)]);
	}
	while (s[i])
	{
		if (s[i] == (char)c)
		{
			return ((char *)&s[i]);
			break ;
		}
		i++;
	}
	return (NULL);
}

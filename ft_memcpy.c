/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmarie-- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 11:08:12 by tmarie--          #+#    #+#             */
/*   Updated: 2021/11/28 15:42:18 by tmarie--         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	size_t	i;
	char	*srcbis;
	char	*destbis;

	srcbis = (char *)src;
	destbis = (char *)dest;
	i = 0;
	if ((srcbis != NULL) && (destbis != NULL))
	{
		while (i < n)
		{
			destbis[i] = srcbis[i];
			i++;
		}
	}
	return (dest);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmarie-- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/30 19:06:55 by tmarie--          #+#    #+#             */
/*   Updated: 2021/11/30 19:15:30 by tmarie--         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t			i;
	unsigned char	*str;
	void			*matchingbyte;

	i = 0;
	str = (unsigned char *)s;
	matchingbyte = NULL;
	while (i < n)
	{
		if (str[i] == (unsigned char)c)
		{
			matchingbyte = &str[i];
			break ;
		}
		i++;
	}
	return (matchingbyte);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmarie-- <tmarie--@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/20 19:19:16 by tmarie--          #+#    #+#             */
/*   Updated: 2021/12/20 20:51:24 by tmarie--         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	ft_putnbr(int n)
{
	unsigned int	nbr;

	nbr = n;
	if (nbr == 0)
		return (0 + '0');
	if (nbr > 0)
	{
		ft_putnbr(nbr / 10);
		nbr = nbr % 10;
	}
	return (nbr + '0');
}

static size_t	ft_getlen(int n)
{
	size_t	len;
	unsigned int	nbr;
	len = 0;
	if (n < 0)
	{
		len++;
		nbr = (unsigned int)-n;
	}
	else
		nbr = (unsigned int)n;
	while (nbr > 9)
	{
		nbr = nbr / 10;
		len++;
	}
	len++;
	return (len);
}

char	*ft_itoa(int n)
{
	char	*result;
	size_t	len;
	unsigned int	num;
	int		i;

	i = 0;
	len = ft_getlen(n);
	result = malloc(sizeof(char) * (len + 1));
	if (result == NULL)
		return (NULL);
	if (n < 0)
		num = (unsigned int)-n;
	else
		num = (unsigned int)n;
	while (i++ <= (int)len - 1)
	{
		result[(int)len - i] = ft_putnbr(num);
		num = num / 10;
	}
	if (n < 0)
		result[0] = '-';
	result[len] = '\0';
	return (result);
}

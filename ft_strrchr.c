/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmarie-- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 11:53:18 by tmarie--          #+#    #+#             */
/*   Updated: 2021/12/14 09:34:11 by tmarie--         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int		i;
	char	*str;

	str = (char *)s;
	i = ft_strlen(str);
	if (c == 0)
		return ((char *)&s[i]);
	while (i >= 0)
	{
		if (str[i] == (char)c)
			return ((char *)&s[i]);
		i--;
	}
	return (NULL);
}

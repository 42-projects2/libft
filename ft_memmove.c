/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmarie-- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/30 18:15:01 by tmarie--          #+#    #+#             */
/*   Updated: 2021/12/15 11:13:34 by tmarie--         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	char	*dst;
	char	*srcbis;

	dst = (char *)dest;
	srcbis = (char *)src;
	if (!dst && !srcbis)
		return (NULL);
	if (srcbis < dst)
	{
		while (n--)
			dst[n] = srcbis[n];
	}
	else
		ft_memcpy(dst, srcbis, n);
	return (dest);
}

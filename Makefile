# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tmarie-- <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/11/25 10:06:26 by tmarie--          #+#    #+#              #
#    Updated: 2021/11/26 10:24:32 by tmarie--         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
CC=cc
NAME=libft.a
CFLAGS=-Wall -Wextra -Werror
OPTIONS=-c -I.
SRC=*.c
OBJ=*.o

all: $(NAME)

$(NAME): $(OBJ)
		ar -crs $(NAME) $(OBJ)


$(OBJ): $(SRC)
		$(CC) $(CFLAGS) $(OPTIONS) $(SRC)

clean:
		rm -f $(OBJ)

fclean: clean
		rm -f $(NAME)

re: fclean all

.PHONY: clean fclean all re
